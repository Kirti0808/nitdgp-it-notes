#include<stdio.h>
#include<stdlib.h>
int main()
{
int t,c,i,j,swapped,*a;
scanf("%d",&t);
a=(int*)malloc(t*sizeof(int));
for(i=0;i<t;i++)
scanf("%d",a+i);
do
{
swapped=0;
for(i=1;i<t;i++)
{
    if(*(a+i-1)>*(a+i))
    {
    	c=*(a+i-1);
    	*(a+i-1)=*(a+i);
    	*(a+i)=c;
    	swapped=1;
    }
}
}
while(swapped);
for(j=0;j<t;j++)
printf("%d\n",*(a+j));
return 0;
}
