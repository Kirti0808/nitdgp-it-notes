echo "normal"
for i in 1 2 3 4 5
do
echo "$i"
done
echo "{a..b}"
for i in {1..5}
do
echo "$i"
done
echo "seq"
for i in `seq 5`
do
echo "$i"
done
echo "Bash Version ${BASH_VERSION}..."
for i in {1..5..2}
do
echo "$i"
done
