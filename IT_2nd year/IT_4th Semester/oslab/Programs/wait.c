#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#define max_ 5

void ChildProcess(char *number)
{
int pid,i;
pid=getpid();
for(i=0;i<max_;i++)
printf("%d %d\n",pid,i);
}

void main()
{
int pid,pid2,status;
if((pid=fork())<0)
exit(1);
else if(pid==0)
{
ChildProcess("First");
if((pid2=fork())<0)
exit(1);
else if(pid2 == 0)
ChildProcess("Second");
}
pid=wait(&status);
printf("firtst %d\n",pid);
pid=wait(&status);
printf("second %d\n",pid);
}
