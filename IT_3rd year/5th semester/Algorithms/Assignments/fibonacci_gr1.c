#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<iostream>
using namespace std;
#define phi (1+sqrt(5))/2.0
double pow(int n)
{
	double r;
	if(n==0) return 1;
	r = pow(n/2);
	r = r*r;
	if(n%2) r = r*phi;
	return r;
}
int main()
{
	int a,b;
	scanf("%d",&a);
	b=pow(a)/sqrt(5)+0.5;
	printf("%d\n",b);
	return 0;
}
