#include<cstdio>
#include<cstdlib>
#include<iostream>
using namespace std;
int pow(int a, int b)
{
	int r;
	if(b==0) return 1;
	r = pow(a,b/2);
	r = r*r;
	if(b%2) r = r*a;
	return r;
}
int main()
{
	int a,b,n=1,num,den;
	double ans=1;
	scanf("%d %d",&a,&b);
	while(ans>0.0000001)
	{
		num=pow(n,b);
		den=pow(a,n);
		ans=(double)num/den;
		printf("%.6lf\n",ans);
		n++;
	}
	return 0;
}
