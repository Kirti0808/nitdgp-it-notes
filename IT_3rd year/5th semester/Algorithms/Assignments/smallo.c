#include<stdio.h>
#define ll long long int
ll pw(ll a, ll b){
  ll r;
  if(b==0) return 1;
  r = pw(a,b/2);
  r = r*r;
  if(b%2) r = r*a;
  return r;
}
int main()
{
	ll a,b,n=1,num,denom;
	double ans=1;
	scanf("%lld %lld",&a,&b);
	while(ans>0.0000001)
	{
		num=pw(n,b);
		denom=pw(a,n);
		printf("%lld %lld\t",num,denom);
		ans=(double)num/denom;
		printf("%.6lf\n",ans);
		n++;
	}
	return 0;
}
