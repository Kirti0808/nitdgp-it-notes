lxi h,0050h
mvi b,08h
mvi a,40h

loop: cmp m
      jz zero
      inx h
      dcx b
      jnz loop

mvi a,0ffh
out 01

zero: mov a,h
      out 00
      mov a,l
      out 01
      hlt

hlt