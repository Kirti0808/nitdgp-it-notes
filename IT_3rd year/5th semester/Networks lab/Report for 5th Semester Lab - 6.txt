Applications of server-client based socket:

1. FTP(File Transfer Protocol): It is a standard network protocol used to transfer files from one host to another host over a TCP-based network.
The ftp server runs on servers 20 and 21. One port is for file transfer which gets activated only during  file transfer and other is for control which is always on.

Commands:

Start/Stop Server: service vsftpd start/restart/status/stop
Download file: get /path/file
Upload file: put /path/file
Download multiple file: mget /path/file /path/file2
Upload multiple file: mput /path/file /path/file2

2. Telnet - It's a remote login application. The Telnet program can be used to access someone else's computer through one's own computer. One can write commands through the Telnet program on their computer which will be executed on the other person's terminal. This enables one to control the server and communicate with other servers on the network. telnetd represents the telnet server and telnet represents the telnet client.
Telnet runs on port 23.

Commands:

Start Server: sudo /etc/init.d/openbsd-inetd restart
Remote login: telnet ipaddress

3. SSH(Secure Shell): It's similar to telnet but is better since the data transferred through it is encrypted unlike telnet where everything is sent in plaintext. Data transferred is first encrypted at the server with the client's public key and decrypted at the client based on the private key. It's slower than telnet.

4. DHCP(The Dynamic Host Configuration Protocol): It is a standardized networking protocol used for configuring a network and for assigning dynamic IP addresses to devices on a network.
It can be used to confine a device to a particular IP and also to filter/block IPs.
The DHCP server/client configuration is in /etc/dhcp/dhcp.conf file.