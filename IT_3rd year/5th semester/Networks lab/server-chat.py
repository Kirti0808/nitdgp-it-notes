#!usr/bin/env python
import socket
import os
HOST = 'localhost'
PORT = 5006
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept() #addr is the addressof the client along with the port address
print 'connect by', addr, repr(conn)
# O/P connect by ('127.0.0.1', 51217) <socket._socketobject object at 0x7f4d646d9600>
# 51217 is the lowest unused port number chosen by the OS

child_pid = os.fork()


if child_pid == 0:
	while True:
		data = conn.recv(100)
		if len(data) != 0:
			print "client>" + data

else:
	while True:
		send_data = raw_input()
		conn.send(send_data)

conn.close()


# Traceroute