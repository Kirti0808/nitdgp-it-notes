clc;
f = [2,4,5];
coeff = [2,1,1;3,2,-1;5,-4,2;1,3,3];
const = [6;4;12;16];
sign = [-1;1;-1;-1];
primal = [-1,1,1];
primalt = ones(1,size(coeff,1));
f = -f;
for i=1:size(sign)
    if(sign(i)==1)
        coeff(i,:) = -coeff(i,:);
        const(i,:) = -const(i,:);
        primalt(i)=-1;
        sign(i) = -1;
    end
end
coefft = coeff';
constt = f';
ft = const';
signt = zeros(1,size(coefft,1));
for i=1:size(signt,2)
    if(primal(i)==-1)
        signt(i)=-1;
    else
        signt(i)=1;
    end
end
disp(ft);
disp(coefft);
disp(signt);
disp(constt);
disp(primalt);