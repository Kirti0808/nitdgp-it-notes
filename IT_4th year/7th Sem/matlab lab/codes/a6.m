clc;
a=[12,18,6,25;8,7,10,18;14,3,11,20];
cap=[200;500;300];
dem=[180,320,100,400];
b=a;
maxi=max(max(a));
a=maxi-a;
cost = 0;
while(min(min(a))~=inf)
    index = find(a==min(min(a)),1);
    [row, col] = ind2sub(size(a),index);
    sub = min(cap(row), dem(col));
    cap(row) = cap(row)-sub;
    dem(col) = dem(col)-sub;
    cost = cost + b(row, col)*sub;
    if(cap(row)==0)
        a(row,:)=inf;
    end
    if(dem(col)==0)
        a(:,col)=inf;
    end
end
disp(cost);