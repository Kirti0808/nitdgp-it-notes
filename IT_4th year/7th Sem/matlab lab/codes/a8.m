clc;
a=[28,17,26;19,12,16];
supply = [500;300];
demand = [250,250,500];
if(sum(supply)~=sum(demand))
   if(sum(supply)<sum(demand))
       a(size(supply,1)+1,:)=0;
       supply(size(supply,1)+1,:)=sum(demand)-sum(supply);
   else
       a(:,size(demand,2)+1)=0;
       demand(size(demand,2)+1,:)=sum(supply)-sum(demand);
   end
end
b = zeros(3,3);
c = a;
cost = 0;
while(max(max(supply),max(demand))~=0)
    penalty_row = zeros(size(a,1),1);
    penalty_col = zeros(1,size(a,2));
    for i=1:size(a,1)
        if(min(setdiff(a(i,:),min(a(i,:))))~=inf)
            penalty_row(i) = min(setdiff(a(i,:),min(a(i,:))))-min(a(i,:));
        end
    end
    for i=1:size(a,2)
        if(min(setdiff(a(:,i),min(a(:,i))))~=inf)
            penalty_col(i) = min(setdiff(a(:,i),min(a(:,i))))-min(a(:,i));
        end
    end
    max_r=max(penalty_row);
    max_c=max(penalty_col);
    if(max_r>max_c)
        index = find(penalty_row==max_r,1);
        min_el = min(a(index,:));
        in = find(a(index,:)==min_el);
        sub = min(demand(in),supply(index));
        b(index,in)= sub;
        demand(in) = demand(in)-sub;
        supply(index) = supply(index)-sub;
        if(supply(index)==0)
            a(index,:)=inf;
        else
            a(:,in)=inf;
        end
    else
        index = find(penalty_col==max_c,1);
        min_el = min(a(:,index));
        in = find(a(:,index)==min_el);
        sub = min(demand(index),supply(in));
        b(in,index)= sub;
        demand(index) = demand(index)-sub;
        supply(in) = supply(in)-sub;
        if(demand(index)==0)
            a(:,index)=inf;
        else
            a(in,:)=inf;
        end
    end
    cost = cost + min_el*sub; 
end
disp(b);
disp(cost);
opt=size(find(b~=0),1)==size(a,1)+size(a,2)-1;
u=inf(3,1);
v=inf(1,3);
u(1)=0;
for i=1:3
    for j=1:3
        if(b(i,j)~=0)
            if(u(i)~=inf)
                v(j)=c(i,j)-u(i);
            elseif(v(j)~=inf)
                u(i)=c(i,j)-v(j);
            end
        end    
    end
end
flag=0;
for i=1:size(b,1)
    for j=1:size(b,2)
        if(c(i,j)-u(i)-v(j)<0)
            flag=1;
        end
    end    
end
if flag==0
    disp('Optimal');
end